requires 'Path::Tiny', '0.076';
requires 'Ref::Util';
requires 'Exporter::Tiny', 1;

on test => sub {
    requires 'Test2::Tools::Spec';
    requires 'Test2::V0';
};

on develop => sub {
    requires 'Test::Pod';
};
